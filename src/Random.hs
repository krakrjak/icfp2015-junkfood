{-# LANGUAGE FlexibleContexts #-}
-- | Linear conguential generator as specified by the ICFP PC 2015.
module Random where

import Control.Applicative ((<*>))
import Control.Monad.State.Class (MonadState(state))

import Data.Bits (shiftR, (.&.))
import Data.Functor ((<$>))
import Data.Word (Word32)

type Seed = Word32

multiplier :: Seed
multiplier = 1103515245

increment :: Seed
increment = 12345

{-| "The random number associated with a seed consists of bits 30..16 of that
 - seed, where bit 0 is the least significant bit."
 -
 - We convert the result to an 'Int' because it is used as a
 - 'Data.Vector.Vector' index.
 -}
currentValue :: Seed -> Int
currentValue seed = fromIntegral $ shiftR seed 16 .&. 0x7FFF

nextSeed :: Seed -> Seed
nextSeed = (+ increment) . (multiplier *)

{- | Similar to 'Random.next', good start on an implementation of that
 - typeclass, too.
 -}
next :: Seed -> (Int, Seed)
next = (,) <$> currentValue <*> nextSeed

genNext :: MonadState Seed m => m Int
genNext = state next
