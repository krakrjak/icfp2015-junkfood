{-# LANGUAGE NamedFieldPuns, BangPatterns #-}
module Game
 ( GameState(..)
 , commandUnit, makeGames, makeMove, powerScore
 , prepForSpawn, moveScore, validLocation, validCellLocation
 , ppGameState
 )
where

import Control.Monad (replicateM)
import qualified Control.Monad.State as State
import Control.Monad.State (evalState, execState, state)
import Control.Lens (ix, zoom, (%~), (.=), (+=), (<<.=), (&))

import qualified Data.Foldable as F
import Data.Functor ((<$>))
import Data.List (tails, intersperse, isPrefixOf)
import Data.Maybe (fromMaybe)
import qualified Data.Set as S
import Data.Set (Set)

import qualified Data.Vector as V
import Data.Vector (Vector, (!), (!?))

import Random
import Types

data GameState = GameState
	{ board :: !Board
	, playerUnit :: !Unit
	, currentMoveScore :: Integer
	, previousUnitCells :: [Set Cell]
	, previousLinesCleared :: !Height
	, sourceUnits :: ![Unit] -- ^ Spawn-ready, in this order
	} deriving (Eq, Read, Show)

{-
 - . @ @ . .   Next:
 -  . . @ . .  0 0
 - . . . . .      0
 -  . . . . .  1
 - . . . . .   2
 -  . . . . .   2
 - . . . . .       3
 -  . . . . .   3 3
 - # # . . #   4 4 4
 -  # # . # #
 - Score: 12345  PLC: 0
 -}
ppGameState :: GameState -> String
ppGameState gs = unlines $ zipWith dblSpc pBoard (nextHead : repeat "") ++ [dblSpc scoreStr plcStr]
 where
  nextHead = "Next:"
  scoreStr = "Score: " ++ (show $ currentMoveScore gs)
  plcStr   = "PLC: " ++ (show $ previousLinesCleared gs)
  dblSpc l r = l ++ "  " ++ r
  locked = (fmap.fmap) (\x -> if x then '#' else '.') (board gs)
  player = playerUnit gs
  incPlayer = markGBoard '@' (members player) locked
  ppivot = pivot player
  incPivot = incPlayer & ix (y ppivot) . ix (x ppivot) %~ (\c -> if c == '@' then '%' else '*')
  pRow y row = replicate (y `mod` 2) ' ' ++ intersperse ' ' (V.toList row) ++ replicate ((y + 1) `mod` 2) ' '
  pBoard = V.toList $ V.imap pRow incPivot

board_ :: Functor f => (Board -> f Board) -> GameState -> f GameState
board_ f gs@GameState{board} = fmap (\board' -> gs { board = board' }) (f board)

previousLinesCleared_ :: Functor f => (Height -> f Height) -> GameState -> f GameState
previousLinesCleared_ f gs@GameState{previousLinesCleared} =
	fmap (\previousLinesCleared' -> gs { previousLinesCleared = previousLinesCleared' }) (f previousLinesCleared)

currentMoveScore_ :: Functor f => (Integer -> f Integer) -> GameState -> f GameState
currentMoveScore_ f gs@GameState{currentMoveScore} =
	fmap (\currentMoveScore' -> gs { currentMoveScore = currentMoveScore' }) (f currentMoveScore)

previousUnitCells_ :: Functor f => ([Set Cell] -> f [Set Cell]) -> GameState -> f GameState
previousUnitCells_ f gs@GameState{previousUnitCells} =
	fmap (\previousUnitCells' -> gs { previousUnitCells = previousUnitCells' }) (f previousUnitCells)

-- Having board be a Vector of rows instead of a Vector of columns might
-- prevent some listification and transposition.
clearLines :: Board -> (Height, Board)
clearLines b = (c, V.fromList $ (replicate c $ V.replicate w False) ++ k)
 where
	accum row (!cleared, kept) | V.and row = (cleared + 1,   kept)
	accum row (!cleared, kept) | otherwise = (cleared, row : kept)
	(c, k) = V.foldr accum (0, []) b
	w = V.length (b ! 0)

makeMove :: UnitCommand -> GameState -> Either Integer GameState
makeMove cmd gs = if lock
	then let gs' = flip execState gs $ do
			linesCleared <- zoom board_ $ do
				State.modify $ markBoard unitMembers
				state clearLines
			plc <- previousLinesCleared_ <<.= linesCleared
			currentMoveScore_ += moveScore (V.length unitMembers) linesCleared plc
			previousUnitCells_ .= []
		in case sourceUnits gs' of
		 (h:t) | validLocation (board gs') h -> Right gs' { playerUnit = h, sourceUnits = t }
		 _                                   -> Left $ currentMoveScore gs'
	else let unitCells = S.fromList $ V.toList unitMembers in
		if unitCells `elem` previousUnitCells gs
			then Left 0
			else Right gs { playerUnit = unit'
			              , previousUnitCells = unitCells : previousUnitCells gs
			              }
 where
	unitMembers = members unit
	unit = playerUnit gs
	unit' = commandUnit cmd unit
	lock = not $ validLocation (board gs) unit'

-- | Sorry, some games end before they are started.  If the sourceLength is < 1
-- or the spawn area for the first unit is filled, the game ends before we even
-- send a command.
makeGames :: Input -> Vector (Maybe GameState)
makeGames inp = fmap makeGame (sourceSeeds inp)
 where
	getRandomData = replicateM (sourceLength inp) genNext
	us = units inp
	selectUnit rand = us ! (rand `mod` V.length us)
	ib = initialBoard (width inp) (height inp) (filled inp)
	makeGame seed =
		case selectedUnits of
		 (h:t) | validLocation ib h -> Just GameState
			{ board = ib
			, playerUnit = h
			, currentMoveScore = 0
			, previousUnitCells = []
			, previousLinesCleared = 0
			, sourceUnits = t
			}
		 _ -> Nothing
	 where
		selectedUnits = map selectUnit $ evalState getRandomData seed

prepForSpawn :: Width -> Unit -> Unit
prepForSpawn w u | V.length cells < 1 = u
                 | dx == 0 && dy == 0 = u
                 | otherwise          = u { members = fmap updateCell cells, pivot = updateCell (pivot u) }
 where
	cells = members u
	makeCellStats Cell{x, y} = (x, x, y)
	accumCellStats (lx1, rx1, ty1) (lx2, rx2, ty2) =
		(min lx1 lx2, max rx1 rx2, min ty1 ty2)
	(lx, rx, ty) = F.foldl1 accumCellStats $ fmap makeCellStats cells
	dy = (-ty)
	dx = (w - rx - lx - 1) `div` 2
	updateCell Cell{x,y} = Cell { x = x + dx, y = y + dy }

commandUnit :: UnitCommand -> Unit -> Unit
commandUnit (Move d) u = Unit { members = fmap (moveCell d) (members u), pivot = moveCell d (pivot u) }
commandUnit (Turn r) u = u { members = fmap (rotateCell r (pivot u)) (members u) }

validLocation :: Board -> Unit -> Bool
--validLocation b u | trace ("validLocation " ++ show b ++ " " ++ show u) False = error "trace"
validLocation b u = V.all (validCellLocation b) (members u)

validCellLocation :: Board -> Cell -> Bool
validCellLocation b c = fromMaybe False $ do
	row <- b !? (y c)
	not <$> row !? (x c)

moveCell :: Direction -> Cell -> Cell
moveCell E  c = c { x = (x c) + 1 }
moveCell W  c = c { x = (x c) - 1 }
moveCell SE c = Cell { x = (x c)     + (y c) `mod` 2, y = (y c) + 1 }
moveCell SW c = Cell { x = (x c) - 1 + (y c) `mod` 2, y = (y c) + 1 }

{- Ugh.  Basically, we covert to a different coordinate system where the axes
 - are straight and independent, but not entirely orthogonal, and the pivot
 - point is the origin.  In this intermediate coordinate system the rotation is
 - easier, so we apply it there.  Finally, we convert back to the original
 - coordinates.
 -}
rotateCell :: Rotation
           -> Cell
           -> Cell -> Cell
rotateCell d p c = case d of
	CW -> c' (-dy) (dy + dx)
	CCW -> c' (dx + dy) (-dx)
 where
	dy = (y c) - (y p)
	dx = (x c) - (x p) - (dy + (y p) `mod` 2) `div` 2
	c' dx' dy' = Cell
		{ y = (y p) + dy'
		, x = (x p) + dx' + (dy' + (y p) `mod` 2) `div` 2
		}

powerScore :: String -- ^ Word of Power
           -> String -- ^ Unit Command Characters
           -> Integer
powerScore p c = 2 * toInteger (length p) * reps + bonus
 where
	reps = toInteger . length $ filter (isPrefixOf p) (tails c)
	bonus | reps > 0  = 300
	      | otherwise = 0
-- Might want do do things "in reverse" ^^

moveScore :: Int -> Height -> Height -> Integer
moveScore size cleared prevCleared = points + bonus
 where
	cInteger = toInteger cleared
	points = toInteger size + 100 * (1 + cInteger) * cInteger `div` 2
	bonus | prevCleared > 1 = toInteger (prevCleared - 1) * points `div` 10
	      | otherwise       = 0


{- Strategy Layout-ish

Apply the following (heuristic)weights for a Hedonistic Algorithm:

    -3.78 for the total sum of heights
    -2.31 per hole
    -0.59 per blockade (a cell above a hole)
    +1.6 per clear
    +3.97 for each edge touching another block
    +6.52 for each edge touching the wall
    +0.65 for each edge touching the floor

Blockade, hole, and counting adjacent tiles are all per-move checking operations
A basic optimization approach: 
    break if cells touching wall == size(current piece)
    else break if cells touching another block or wall  == size(current piece)
    etc.

-}
