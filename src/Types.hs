{-# LANGUAGE NamedFieldPuns, DeriveGeneric #-}

module Types where

import Control.DeepSeq (NFData)
import Control.Monad.ST (runST)

import Data.Aeson (FromJSON, ToJSON)
import qualified Data.Vector as V
import Data.Vector (Vector, (!), thaw, freeze)
import qualified Data.Vector.Mutable as MV

import GHC.Generics (Generic)

import Random (Seed)

-- | Range is [0, height)
type Row = Height

-- | Range is [0, width)
type Column = Width

-- Ord instance may not always be apropos.
data Cell = Cell
	{ x :: !Row
	, y :: !Column
	} deriving (Eq, Ord, Read, Show, Generic)
instance FromJSON Cell

data Unit = Unit
	{ members :: !(Vector Cell)
	, pivot :: !Cell
	} deriving (Eq, Read, Show, Generic)
instance FromJSON Unit

data Direction = E | W | SE | SW deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

data Rotation = CW | CCW deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

data UnitCommand = Move !Direction | Turn !Rotation
	deriving (Eq, Ord, Read, Show, Generic)
-- Enum and Bounded instances would be nice.

type GBoard a = Vector (Vector a)

-- | True = filled; False = empty
type Board = GBoard Bool

-- Int is used to easy create/index Vectors
type Width = Int
type Height = Int

data Input = Input
	{ id :: !Integer
	, units :: !(Vector Unit)
	, width :: !Width
	, height :: !Height
	, filled :: !(Vector Cell)
	, sourceLength :: !Int
	, sourceSeeds :: !(Vector Seed)
	} deriving (Eq, Read, Show, Generic)
instance FromJSON Input

emptyBoard :: Width -> Height -> Board
emptyBoard w h = V.replicate h $ V.replicate w False

markGBoard :: a -> Vector Cell -> GBoard a -> GBoard a
markGBoard v cells board = runST $ do
	mutRows <- V.mapM thaw board
	V.forM_ cells $ \Cell{x, y} ->
		MV.write (mutRows ! y) x v
	V.mapM freeze mutRows

markBoard :: Vector Cell -> Board -> Board
markBoard = markGBoard True

initialBoard :: Width -> Height -> Vector Cell -> Board
initialBoard = (flip markBoard .) . emptyBoard

inputBoard :: Input -> Board
inputBoard Input{width, height, filled} = initialBoard width height filled

data Command = CommandError | CommandIgnored | UnitCommand !UnitCommand
	deriving (Eq, Ord, Read, Show, Generic)
-- Enum and Bounded instances would be nice.

charCommand :: Char -> Command
charCommand c
	| c `elem` "p'!.03" = UnitCommand (Move W)
	| c `elem` "bcefy2" = UnitCommand (Move E)
	| c `elem` "aghij4" = UnitCommand (Move SW)
	| c `elem` "lmno 5" = UnitCommand (Move SE)
	| c `elem` "gqrvz1" = UnitCommand (Turn CW)
	| c `elem` "kstuwx" = UnitCommand (Turn CCW)
	| c `elem` "\t\n\r" = CommandIgnored
	| otherwise         = CommandError

charUnitCommand :: Char -> Maybe UnitCommand
charUnitCommand c = case decoded of UnitCommand uc -> Just uc
                                    _              -> Nothing
    where
        decoded = charCommand c

type Output = Vector SingleOutput

data SingleOutput = Output
	{ problemId :: !Integer
	, seed :: !Seed
	, tag :: !String
	, solution :: !String
	} deriving (Eq, Read, Show, Generic)
instance ToJSON SingleOutput
instance NFData SingleOutput
