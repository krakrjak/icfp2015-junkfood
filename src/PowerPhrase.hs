{-# LANGUAGE ViewPatterns #-}
module PowerPhrase where

import Data.Char (toLower)
import Data.List (sortBy)
import Data.Maybe (mapMaybe)
import Data.Ord (comparing)

import Types hiding (id)

prepToSpeak :: String -> String
prepToSpeak = mapMaybe (acceptable . toLower)
 where
	acceptable (charCommand -> CommandError) = Nothing
	acceptable c                             = Just c

-- Takes a list of moves and tries to add power phrases without changing the moves.
newSpeak :: [String] -> String -> String
newSpeak pps mvs = newSpeak' mvs
 where
	newSpeak' [] = []
	newSpeak' (h:t) = replaceAll (h : newSpeak' t)
	replaceAll = foldr (.) id . map replacePrefix $ sortBy (flip $ comparing length) pps
	replacePrefix pp = helper (unitCommands pp) []
	 where
		helper []            _    moves  = pp ++ moves  -- Match / Replaced
		helper  _            undo []     = reverse undo -- Mismatch (Phrase too long)
		helper cmds@(uc:ucs) undo moves@(m:ms) =
			case charCommand m of
			 UnitCommand mc | mc == uc -> helper ucs (m:undo) ms -- Provisional acceptance
			 UnitCommand _             -> reverse undo ++ moves  -- Mismatch
			 _                         -> helper cmds undo ms    -- Strip
	unitCommands = mapMaybe (\c -> case charCommand c of { UnitCommand uc -> Just uc; _ -> Nothing })
