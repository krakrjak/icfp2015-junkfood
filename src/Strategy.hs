{-# LANGUAGE DeriveFunctor #-}
module Strategy
	( goEast, goWest, goSE, goSW, fourWay, allWay, runStrategy, Strategy
	, hedonisticSimple, strategyToMulti, MultiStrategy, runMulti
	, hyloStrategy
	)
where

import Control.Applicative (Applicative, pure)
import Control.Monad (MonadPlus, filterM)
import Control.Monad.State (State, get, put, runState)

import Data.Either (partitionEithers)
import Data.Either.Combinators (mapBoth)
import qualified Data.Foldable as F
import Data.Foldable (foldMap)
import Data.Functor.Foldable (ghylo, hylo)
import Data.Functor.Identity (Identity(..))
import Data.IORef (newIORef, readIORef, writeIORef)
import Data.Maybe (mapMaybe)
import Data.Monoid (Sum(..), getSum)
import Data.Ord (comparing)
import Data.List (sortBy, maximumBy)
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Vector as V

import System.IO (Handle, hPutStrLn)

import Game
import Types hiding (id)

-- | Second argument starts with most recent; it is reversed from what will be
-- submitted.
type GStrategy f a = GameState -> String -> f a
type MultiStrategy f = GStrategy f (NonEmpty Char)
type Strategy f = GStrategy f Char

strategyToMulti :: Functor f => Strategy f -> MultiStrategy f
strategyToMulti = ((fmap (:|[]) .) .)

stepStrategy :: Monad m => Strategy m -> GameState -> String -> m (Either (Integer, String) (GameState, String))
stepStrategy f gs moves = do
        guessMove <- f gs moves
        let
            guessCmd = charCommand guessMove
            moves' = guessMove : moves
        return $ case tryMove guessCmd of
            Left  score   -> Left (score, moves')
            Right gs'     -> Right (gs', moves')
    where
        tryMove CommandError = Left 0
        tryMove CommandIgnored = Right gs
        tryMove (UnitCommand cmd) = makeMove cmd gs

stepMulti :: Monad m => MultiStrategy m -> GameState -> String -> m (String, Either Integer GameState)
stepMulti f = stepMulti'
 where
	stepMulti' gs moves = do
		first :| plan <- f gs moves
		step <- stepStrategy (\_ _ -> return first) gs moves
		case step of
		 Left (score, moves') -> return (moves', Left score)
		 Right  (gs', moves') ->
			case plan of
			 []    -> return (moves', Right gs')
			 (h:t) -> stepMulti (\_ _ -> return (h :| t)) gs' moves'

goEast :: Monad m => Strategy m
goEast _ _ = return 'b' -- TODO: pick from any of b c e f y 2 instead

goWest :: Monad m => Strategy m
goWest _ _ = return 'p' -- TODO: pick from any of p ' ! . 0 3 intead

goSW :: Monad m => Strategy m
goSW _ _ = return 'a' -- TODO: pick from any of a g h i j 4 instead

goSE :: Monad m => Strategy m
goSE _ _ = return 'l' -- TODO: pick from any of l m n o <space> 5 instead

fourWay :: Monad m => Strategy m
fourWay = firstSafeOr "lab!" safe 'l'
 where
	safe gs _ c =
		return $ case charCommand c of
		 UnitCommand cmd -> wouldNotLock gs cmd
		 _               -> False

allWay :: MonadPlus m => Strategy m
allWay gs _ = do
	return $ case sortBy (comparing fst) plays of
	 [] -> '!' -- (Going to error.)
	 (h:_) -> snd h
 where
	plays = mapMaybe safePlay "burlap"
	safePlay c =
		case charCommand c of
		 UnitCommand cmd ->
			case makeMove cmd gs of
			 Left  0   -> Nothing   -- Duplicate position or other error
			 Left  _   -> Just (True, c) -- Lock ended game.
			 Right gs' -> -- pUC = [] -> locked; new unit
				Just (null (previousUnitCells gs'), c)
		 _ -> Nothing

stepSearch :: (GameState -> String -> Integer) -> State [(Integer, (GameState, String))] (Maybe (String, Integer))
stepSearch score = do
	q <- get
	case q of
	 []                 -> return Nothing
	 ((s,(gs,moves)):t) -> do
		let
			addMove x y = (x : moves, y)
			(closed, open) = partitionEithers
			               $ map (\x -> mapBoth (addMove x) (addMove x) (playMove x gs)) "burlap"
			qNew = map (\(m,gs') -> (score gs' m, (gs', m))) open
		put $ mergeFilter fst (fst . snd) t qNew
		return . Just $ foldl myMax (moves, s) closed
 where
	myMax l@(_, s) r@(_, t) | s < t     = r
	                        | otherwise = l
	playMove c gs =
		case charCommand c of
		 UnitCommand cmd -> makeMove cmd gs
		 _               -> Left 0

mergeFilter :: (Eq k, Ord s) => (a -> s) -> (a -> k) -> [a] -> [a] -> [a]
mergeFilter score key = mergeFilter'
 where
	mergeFilter'    []     r         = r
	mergeFilter' l            []     = l
	mergeFilter' l@(lh:lt) r@(rh:rt)
		| score lh > score rh    = lh : mergeFilter' lt (filter (\x -> key x /= key lh) r)
		| otherwise              = rh : mergeFilter' (filter (\x -> key x /= key rh) l) rt

promoteStateStrategyToIO :: s -> GStrategy (State s) a -> IO (GStrategy IO a)
promoteStateStrategyToIO initial strat = do
	stateCell <- newIORef initial
	return $ \gs moves -> do
		st <- readIORef stateCell
		let (r, st') = runState (strat gs moves) st
		writeIORef stateCell st'
		return r

wouldNotLock :: GameState -> UnitCommand -> Bool
wouldNotLock = (not .) . wouldLock

-- | Makes a whole new GameState, then throws it away; be careful about
-- performance.
wouldLock :: GameState -> UnitCommand -> Bool
wouldLock gs cmd =
	case makeMove cmd gs of
	 Left  _   -> True
	 Right gs' -> null $ previousUnitCells gs'

firstSafeOr :: Monad m => String -> (GameState -> String -> Char -> m Bool) -> Char -> Strategy m
firstSafeOr cmdChars safe defCmdChar gs moves = do
	safeCmds <- filterM (safe gs moves) cmdChars
	return $ case safeCmds of
	 (h:_) -> h
	 []    -> defCmdChar

hedonisticSimple :: Monad m => MultiStrategy m
hedonisticSimple gs _ =
	return $ case fst . foldl combineMove defaultMove $ playN 6 gs of
	 []    -> '!' :| []
	 (h:t) -> h   :| t
 where
	defaultMove = ("!", 0)
	combineMove l@(_, n) r@(_, m) = if n < m then r else l

data AStarF a = AStarF
	{ openSet :: [(Integer, String, GameState)]
	, next :: Maybe a
	, closed :: [(String, Integer)]
	}

data GExploreTree l n = Cutoff l
                      | Expanding (NonEmpty (Either l n))
                      deriving Functor
type ExploreTree = GExploreTree (String, Integer)

exploreTree :: Int -> (GameState -> Integer) -> GameState -> (String, Integer)
exploreTree n score rootGS = hylo {- wdist mdist -} combine expand $ (n, "", rootGS)
 where
	expand :: (Int, String, GameState) -> ExploreTree (Int, String, GameState)
	expand (n, moves, gs) | n < 1 = Cutoff (moves, score gs)
	expand (n, moves, gs) = Expanding $ fmap makePlay ('b' :| "urlap")
	 where
		makePlay c =
			case charCommand c of
			 CommandError   -> Left (moves', 0)
			 CommandIgnored -> Left (moves', score gs) -- Kill recursion to prevent ignored spam.
			 UnitCommand cmd ->
				case makeMove cmd gs of
				 Left  fscore -> Left  (moves', fscore)
				 Right gs'    -> Right (n - 1, moves', gs')
		 where
			moves' = c : moves
	combine :: ExploreTree (String, Integer) -> (String, Integer)
	combine (Cutoff leaf) = leaf
	combine (Expanding list) = F.maximumBy (comparing snd) $ fmap (either id id) list

hyloStrategy :: Applicative m => MultiStrategy m
hyloStrategy gs _ =
	pure $ case fst $ exploreTree 8 huerScore gs of
	 []    -> '!' :| []
	 (h:t) -> h   :| t
 where
	huerScore gs = currentMoveScore gs - filledPenalty (board gs)

playN :: Int -> GameState -> [(String, Integer)]
playN n gs | n < 1 = [("", currentMoveScore gs + (adjustScore (board gs) (playerUnit gs )) )]
playN n gs = do
	firstPlay <- "burlap"
	let
	    (UnitCommand cmd) = charCommand firstPlay -- partial
	case makeMove cmd gs of
	 Left finalScore -> return (firstPlay : [], finalScore)
	 Right gs'       -> do
		(moves, score) <- playN (n - 1) gs'
		return (firstPlay : moves, score)
 where
	oldScore = currentMoveScore gs

adjustScore :: Board -> Unit -> Integer
adjustScore b u = wallBonus + floorBonus + neighborBonus + emptyBonus - heightPenalty
    where
        unitSize = V.length $ members u
        wallBonus = if onWall b u then 5 else 0
        floorBonus = if onFloor b u then 2 else 0
        neighborBonus = 5 * countAdjFilled b u
        emptyBonus = 2 * countAdjEmpty b u
        heightPenalty = 3 * highestFixed b

highestFixed :: Board -> Integer
highestFixed b = case values
                 of Nothing -> 0
                    Just row -> fromIntegral $ V.length b - row
    where
        values = V.findIndex (\x -> case x 
                                    of Just _  -> True
                                       Nothing -> False)
                    $ V.map (\vec -> V.elemIndex True vec) b

pieceHeight :: Board -> Unit -> Integer
pieceHeight b u = fromIntegral $ V.maximum $ V.map (\c -> height - (y c)) $ members u
 where
    height = V.length b

filledPenalty :: Board -> Integer
filledPenalty b = V.ifoldl (\p ry row -> p + getSum (rowPenalty ry row)) 0 b
 where
	h = V.length b
	rowPenalty ry = foldMap cellPenalty
	 where
		perMarkPenalty = h - ry
		cellPenalty v = Sum . toInteger $ fromEnum v * perMarkPenalty

onFloor :: Board -> Unit -> Bool
onFloor b u = V.any (== True) $ V.map (\c -> y c == V.length b) $ members u

onWall :: Board -> Unit -> Bool
onWall b u = atExtremes
    where
        atExtremes = V.any (== True) $ V.map (\c -> x c == 0 || x c == w || y c == h) $ members u
        h = V.length b
        w = V.length $ b V.! 0

countAdjEmpty :: Board -> Unit -> Integer
countAdjEmpty b u = V.foldl summary 0 $ V.concatMap neighborPoints (members u)
    where
        summary acc (xpos, ypos) = if validCellLocation b Cell{ x = xpos, y = ypos }
                                   then if not $ (b V.! ypos) V.! xpos then acc + 1 else acc
                                   else acc

countAdjFilled :: Board -> Unit -> Integer
countAdjFilled b u = V.foldl summary 0 $ V.concatMap neighborPoints (members u)
    where
       summary acc (xpos, ypos) = if validCellLocation b Cell{ x = xpos, y = ypos }
                                  then if (b V.! ypos) V.! xpos then acc + 1 else acc
                                  else acc

neighborPoints :: Cell -> V.Vector (Int, Int)
neighborPoints c = V.fromList $
   [(cellX - 1 + cellY `mod` 2, cellY - 1) -- NW
   ,(cellX     + cellY `mod` 2, cellY - 1) -- NE
   ,(cellX + 1                , cellY    ) -- E
   ,(cellX     + cellY `mod` 2, cellY + 1) -- SE
   ,(cellX - 1 + cellY `mod` 2, cellY + 1) -- SW
   ,(cellX - 1                , cellY    )]-- W
    where
        cellX = x c
        cellY = y c

possibleMoves :: String
possibleMoves = "abcefy2p'!.03aghij4lmno 5gqrvz1kstuwx"

nonLockingMoves :: GameState -> [Char]
nonLockingMoves gs = filter (safe gs) possibleMoves
    where
        safe gs' move = case charCommand move of
            UnitCommand cmd -> wouldNotLock gs' cmd
            _               -> False

lockingMoves :: GameState -> [Char]
lockingMoves gs = filter (unsafe gs) possibleMoves
    where
        unsafe gs' move = case charCommand move of
            UnitCommand cmd -> wouldLock gs' cmd
            _               -> False

runMulti :: Integer -> MultiStrategy IO -> Maybe Handle -> GameState -> IO (Integer, String)
runMulti _timeout f gsh = runMulti' []
  where
        step = stepMulti f
        runMulti' s gs = do
                case gsh of Just gsh' -> hPutStrLn gsh' $ ppGameState gs
                            Nothing   -> return ()
                tryStep <- step gs s
                case tryStep of (moves, Left score) -> return $ (score, moves)
                                (moves, Right gs')  -> runMulti' moves gs'

runStrategy :: Integer -> Strategy IO -> Maybe Handle -> GameState -> IO (Integer, String)
runStrategy timeout = runMulti timeout . strategyToMulti
