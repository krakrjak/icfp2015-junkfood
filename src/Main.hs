module Main (main) where

import qualified Data.ByteString.Lazy as LBS

import Control.Concurrent (forkIO, getNumCapabilities)
import Control.Concurrent.Chan (newChan, writeChan, readChan)
import Control.DeepSeq (($!!))
import Control.Exception (bracket, onException)
import Control.Lens (from, (&), (%~), _1, view)
import Control.Monad (forM_, (>=>) )

import Data.List (maximumBy, nub)
import Data.Ord (comparing)
import qualified Data.Vector as V
import Data.Aeson (FromJSON, ToJSON, eitherDecode, encode)
import Data.Vector.Lens (vector)
import Data.Version (showVersion)

import Options.Applicative

import Random (Seed)

import System.IO
	( stdin, Handle, hPutStrLn, openFile
	, withFile, IOMode ( ReadMode, WriteMode )
	)

import Test.QuickCheck (arbitrary, generate)

import Game
import Types
import PowerPhrase
import Strategy
import Version
import Paths_icfp2015_junkfood (version)

stdinStr :: String
stdinStr = "-"

data Arg = Arg
    { file_name    :: String
    , time_limit   :: Integer
    , memory_limit :: Maybe Integer
    , power_phrase :: [String]
    , cpu_count    :: Maybe Int
    , log_run      :: Bool
    }

handleArgs :: Parser Arg
handleArgs = Arg
    <$> strOption
        ( short 'f'
       <> value stdinStr
       <> metavar "INPUTFILE"
       <> help "Read JSON formatted INPUTFILE" )
    <*> option auto
        ( short 't'
       <> value (300) -- Five Minutes
       <> metavar "TIME"
       <> help "Number of seconds allowed" )
    <*> optional (option auto
        ( short 'm'
       <> metavar "MEGABYTES"
       <> help "MEGABYTES of memory allowed" ))
    <*> many ( strOption
        ( short 'p'
       <> metavar "POWERPHRASE"
       <> help "A POWERPHRASE to score more" ))
    <*> optional (option auto
        ( short 'c'
       <> metavar "CPUS"
       <> help "Count of CPUS to utilize" ))
    <*> switch
        ( short 'l'
       <> help "Use to log run for visualizer" )

-- | Parse fail raises an IOError
hFromJSON :: FromJSON a => Handle -> IO a
hFromJSON = LBS.hGetContents >=> (raiseParseFail . eitherDecode)
 where
	raiseParseFail = either (ioError . userError) return

putJSON :: ToJSON a => a -> IO ()
putJSON = (>> putStrLn "") . LBS.putStr . encode

main :: IO ()
main = do
        optVal <- execParser opts
        let
            fileArg = file_name optVal
            phrases = map prepToSpeak $ power_phrase optVal -- Canonicalizes
        handle <- if fileArg == stdinStr then return stdin else openFile fileArg ReadMode
        input  <- hFromJSON handle :: IO Input
        let input' = input -- Canonicalizes
                { units = fmap (prepForSpawn $ width input) (units input)
                , sourceSeeds = sourceSeeds input & from vector %~ nub
                }
            games  = makeGames input'
        caps <- case cpu_count optVal of Nothing -> getNumCapabilities
                                         Just c  -> return c
        ticketDispenser <- newChan
        forM_ [0..caps] $ \t -> writeChan ticketDispenser t
        outputCollection <- newChan
        let forkData = V.zip (sourceSeeds input') games
        V.forM_ forkData $ \(s, mg) -> forkIO $ do
            bracket (readChan ticketDispenser) (\t -> writeChan ticketDispenser t) $ \t -> do
                tagEnd <- generate arbitrary
                let
                    failOutput = Output 
                        { problemId = Types.id input'
                        , seed = s
                        , tag = showVersion version ++ ":" ++ gitVersion ++ "; " ++ tagEnd
                        , solution = ""
                        }
                flip onException (writeChan outputCollection failOutput) $ do
                    withFile (show (Types.id input') ++ "." ++ show s ++ ".gs") WriteMode $ \gsh -> do
                        hPutStrLn gsh $ "Id: " ++ show (Types.id input') ++ "; Seed: " ++ show s ++ "; Ticket: " ++ show t
                        let problemid      = Types.id input
                            timeout        = time_limit optVal
                            testStrategies = [ ("allWay", strategyToMulti allWay)
                                             , ("fourWay", strategyToMulti fourWay)
                                             , ("hedonistic", hedonisticSimple)
                                             , ("hylo", hyloStrategy)
                                             ]
                        singleOutput <- case mg of
                          Just g  -> if log_run optVal
                                     then playAGame problemid s g testStrategies tagEnd (Just gsh) timeout
                                     else playAGame problemid s g testStrategies tagEnd Nothing timeout
                          Nothing -> do
                              hPutStrLn gsh "Game ended before it began."
                              return failOutput
                        let singleOutput' = singleOutput { solution = newSpeak phrases $ solution singleOutput }
                        writeChan outputCollection $!! singleOutput'
        output <- V.forM forkData $ \_ -> do
            readChan outputCollection
        putJSON output
    where
        opts = info (helper <*> handleArgs)
            ( fullDesc
           <> progDesc "icfp2015-junkfood entry"
           <> header "play more games" )

playAGame :: Integer -> Seed -> GameState -> [(String, MultiStrategy IO)] -> String -> Maybe Handle -> Integer -> IO SingleOutput
playAGame i s gs strategies tagEnd gsh timeout = do
    results <- mapM playOne strategies
    let (bestScore, bestMoves, strategyName) = maximumBy (comparing $ view _1) results
    case gsh of Just gsh' -> hPutStrLn gsh' $ "Expected Score: " ++ show bestScore ++ "(" ++ show strategyName ++ ")"
                Nothing -> return ()
    return Output
        { problemId = i
        , seed = s
        , tag = showVersion version ++ ":" ++ gitVersion ++ "; " ++ tagEnd
        , solution = reverse bestMoves
        }
    where
        playOne (name, strat) = do
            case gsh of Just gsh' -> hPutStrLn gsh' $ "Beginning " ++ name ++ " strategy."
                        Nothing -> return ()
            (score', m) <- runMulti (timeout * 1000000) strat gsh gs
            return (score', m, name)
{-
do
	cpus = num_cpus optVal
	capus <- maybe getNumCapabilities return
	forM_ runners $ \r -> do
		forkIO $ act r

	forM_ runners $ \r -> do
		putMVar (stop r) "Time's up"

	loop []               []    results = return results
	loop []               retry results = loop retry [] results
	loop (current:remain) retry results = do
		m_result <- tryTakeMVar current
		case m_result of
		 Nothing -> loop remain (current:retry) r
		 Just r  -> loop remain retry (r:results)
-}
