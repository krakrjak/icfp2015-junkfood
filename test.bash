#!/bin/bash
for i in {0..24}
do
    echo "Testing Problem ${i}"
    ./play_icfp2015 -f doc/qualification_problems/problem_"${i}".json \
        -p 'Yuggoth' -p 'Ei!' -p 'Ia!' -p "Chuthulu" -p "Chuthulu Chuthulu" -p "Chuthulu Chuthulu Chuthulu" -p "R'lyeh" \
        -l > /dev/null
done

echo "Winning Strategies are as follows:"
grep Expected *.gs | cut -d':' -f 3|cut -d'(' -f2|cut -d')' -f1|sort|uniq -c
