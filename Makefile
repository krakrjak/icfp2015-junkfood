.POSIX:

play_icfp2015: dist/build/icfp2015-junkfood/icfp2015-junkfood
	ln -f $< $@ || cp $< $@

dist/build/icfp2015-junkfood/icfp2015-junkfood: src/Version.hs
	cabal build -j

src/Version.hs: always-out-of-date
	printf 'module Version where gitVersion :: String; gitVersion = "%s"\n' $$(git describe --always --dirty --abbrev=4) > src/Version.hs

submit: play_icfp2015
	./submit.bash

test: play_icfp2015
	./test.bash

clean:
	cabal clean
	rm -f *.gs

include make.deps

always-out-of-date:
