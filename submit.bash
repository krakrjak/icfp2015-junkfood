#!/bin/bash
for i in {0..24}; do
	curl --user :$(cat api.key) -X POST -H "Content-Type: application/json" \
		-d "$(\
			./play_icfp2015 -f doc/qualification_problems/problem_${i}.json -p 'Yuggoth' -p 'Ei!' -p 'Ia! Ia!' -p "R'lyeh" -p "Chuthulu" -p "Chuthulu Chuthulu" -p "Chuthulu Chuthulu Chuthulu" \
		)" https://davar.icfpcontest.org/teams/21/solutions
done

